import('./scripts/scrollHinter.js')
.then(
    ({default: ScrollHinter}) => {
        new ScrollHinter();
    }
);
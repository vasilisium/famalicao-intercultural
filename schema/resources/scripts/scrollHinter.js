export default class ScrollHinter {
    constructor(){
        this.launchScrollHintCycle();
    }

    getScrollHinter(){
        const elementId = 'scrollHinter',
        element = document.getElementById(elementId);
        return element.style;
    }
    
    scrollHintCycle(scrollHinter){
        //fade in
        scrollHinter.opacity = 1;
        scrollHinter.transform = 'translate(-50%, 0)'
        
        //fade out
        setTimeout(function(){
            scrollHinter.opacity = 0;
            scrollHinter.transform = 'translate(-50%, 10px)'
        }, 500);
    }
    
    launchScrollHintCycle(){
        let scrollHinter = this.getScrollHinter();
        
        setTimeout(
            setInterval(
                () => {
                    this.scrollHintCycle(scrollHinter);
                }, 
                4000
            ), 
            1000*10
        )
    }
}
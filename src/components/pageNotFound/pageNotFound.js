import React from 'react';

function PageNotFound({ location }) {
    return (
        <div>
            <h1>Error 404</h1>
            <p style={{color:'var(--red)'}}>{`Page "${location.pathname}" you requested, is not existing.`}</p>
        </div>
    )
}

export default PageNotFound

import React from 'react';
import './loadingPage.css'

import { fadeOut } from 'utils/animations';
import config from 'utils/config';

import Logo from 'components/logo';

const LoadingPage = (props) => {
    return ( fadeOut(
        (<div style={props}>
            <div className='splashScreen'>
                <div className="center">
                    <h1>{config.title.title}</h1>
                    <Logo style={{width: '400px'}}/>
                    <p>We are welcome you:)</p>
                </div>
            </div>
        </div>), 
        {delay: config.minLoadTime-300, duration: 300}
    ))
    
}

export default LoadingPage;
import React from 'react';
import {Link} from 'react-router-dom';

import {creationDate} from 'utils/functions';

const PostCard = ({entry}) => {
    const fields = entry.fields;
    const postLinkPart = '/blog/'
    const shortText = (content=fields.text.content) =>{
        if (content.length===0) return '';
        let tmp = '';
        for (let i = 0; i<content.length; i++){
            if (content[i].nodeType){
                if (content[i].nodeType === "paragraph"){
                    tmp = content[i].content;
                    break;
                }
            }
        }

        if(tmp.length>0) if(tmp[0].nodeType==="text") {
            tmp=tmp[0].value;
        }

        tmp=tmp.split('. ').slice(0,2).join('. ') + '...';

        return tmp;
    }
    

    return (
        <div className='postCard'>
            <Link className='postCardTitle' to={postLinkPart + fields.slug}>{fields.title}</Link>
            <div className='shortText'>
                {shortText()}
                <Link className='postReadButton' to={postLinkPart + fields.slug}>Read more</Link>
            </div>
            <div className='postCardcreationDate'>
                Created at: {creationDate(entry)}
            </div>
        </div>
    )
}

export default PostCard;
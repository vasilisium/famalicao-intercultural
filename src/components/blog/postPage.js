import React from 'react';
import {renderRichText} from 'utils/functions';

import MainContext from 'utils/context';

import PageNotFound from 'components/pageNotFound/pageNotFound';

const PostPage = (props) => {
    const slug = props.slug;
    const api = React.useContext(MainContext);
    
    const [post, setPost] = React.useState();
    const [isLoaded, setIsLoaded] = React.useState(false);
    const fields = post ? post.fields : undefined;

    React.useEffect(
        () => {
            if(!api.blog) api.initBlog(
                res=>{setPost(res); setIsLoaded(true)},
                er=>{console.log(er); setIsLoaded(true)},
                slug
            )
            else api.blog.getPostBySlug(
                res=>{setPost(res); setIsLoaded(true)},
                er=>{console.log(er); setIsLoaded(true)},
                slug
            )
            
        }, [api, api.blog, slug]
    )

    return fields ? (
        <div className='postPage'>
            <div className='postTitle'>{fields.title}</div>
            <div className='postText'>{renderRichText(fields.text)}</div>

            {/* post */}
        </div>
    ) 
    : isLoaded && <PageNotFound {...props}/>
        
    
}

export default PostPage;
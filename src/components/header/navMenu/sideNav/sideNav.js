import React from 'react';
import { Link } from 'react-router-dom';

import './sideNav.css';

// import {staticLinks} from '../staticLinks';

const SideNav = (props) => {
    const {pages} = props;
    const cssClassName ='sideMenuItem';
    const linksCustomProps = {
        onClick: props.linkClickHandler,
    }

    const renderPages = (pages) => {
        if (pages){
            return (
                <>
                    {Object.entries(pages).map(([slug, page]) =>
                        <Link className={cssClassName} key={page.id} to={`/${slug}`}
                            {...linksCustomProps}
                        >
                            {page.name}
                        </Link>
                    )}
                    {/* {staticLinks(cssClassName, linksCustomProps)} */}
                </>
            )
        }
    }

    return (
        <nav className='sideNav'>
            {renderPages(pages)}
        </nav>
    )
}

export default SideNav;
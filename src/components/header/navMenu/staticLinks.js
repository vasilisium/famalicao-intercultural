import React from 'react';
import { Link } from 'react-router-dom';

export const staticPages = {
    '/events': 'Events',
}

export const staticLinks = (cssClassName, customProps) =>{
    const links = Object.entries(staticPages).map(([slug, title]) => 
        <Link className={cssClassName} key={slug} to={slug}
            {...customProps}
        >
            {title}
        </Link>
    )

    return links;
}
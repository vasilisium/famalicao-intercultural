import React from 'react';
import { Link } from 'react-router-dom';

import './topNav.css';

// import {staticLinks} from '../staticLinks';

const TopNav = (props) => {
    const {pages} = props;
    const cssClassName ='topMenuItem';

    const renderPages = (pages) => {
        if (pages){
            return (
                <>
                    {Object.entries(pages).map(([slug, page]) =>
                        <Link className={cssClassName} key={page.id} to={`/${slug}`}>
                            {page.name}
                        </Link>
                    )}
                    {/* {staticLinks(cssClassName)} */}
                </>
            )
        }
    }

    return (
        <nav className='topNav'>
            {renderPages(pages)}
        </nav>
    )
}

export default TopNav;
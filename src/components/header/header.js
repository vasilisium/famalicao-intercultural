import React, {useContext} from 'react'
import './header.css';
import { Link } from 'react-router-dom'


import MainContext from 'utils/context';

import Logo from 'components/logo';
import TopNav from 'components/header/navMenu/topNav/topNav'; 
import SideNav from 'components/header/navMenu/sideNav/sideNav'; 
import config from 'utils/config';

const Title = (props) => (
    <div className="title">
        <div className="titleText">
            {props.title}
        </div>
        <div className="titleDivider">
            <div className="dividerTop"></div>
            <div className="dividerBottom"></div>
        </div>
        <div className="titleSlogan">
            {props.slogan}
        </div>
    </div>
)

const Header =  (props) => 
{    
    const api = useContext(MainContext);

    const modalContent = () => (
        <div className='sideMenu'>
            <div className='closeSidemMenuButtom' onClick={props.closeMenuButtonHandler}/>
            <SideNav pages={api.mainMenu} linkClickHandler={props.closeMenuButtonHandler}/>
        </div>
    );
    
    return (
        <header>
            <Link to={config.pages.startPage} className='titleLink'>
                <HeaderLogo/>
                
                <Title title={props.title} slogan={props.slogan} />
            </Link>
            
            <div className='menu'>
                <div className='nav'>
                    <TopNav pages={api.mainMenu}/>
                </div>
                <div className="menuButtom" onClick={() =>{props.menuButtonClickHandler(modalContent)}}>
                    <div className='line'/>
                    <div className='line'/>
                    <div className='line'/>
                </div>
            </div>
        </header>
)}

const HeaderLogo = (props) => <Logo style={{height:'7vh', gridArea:'logo'}}/>

export default Header;
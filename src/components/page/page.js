import React, {useContext, useEffect, useState} from 'react';
import PropTypes from 'prop-types';

import {renderRichText} from 'utils/functions';
import MainContext from 'utils/context';
import {
    isObjectHasField, 
    getFieldOfObjectType, 
} from 'api/helpers';

import './page.css';

const Page = (props) => {
    const slug = props.slug
    const api = useContext(MainContext);

    const [data, setData] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(
        ()=>{
            api.getPage(
                slug,
                data => {
                    setData(data)
                    setIsLoaded(true);
                }, 
                error => console.log(error)
            )
        },
        [api, slug, data, ]
    )

    const getField = (fieldName) => getFieldOfObjectType(data, 'Entry', fieldName)
    const isHasImage = () => isObjectHasField(data, 'image')
    const getImage = () => {
        const imageField = getFieldOfObjectType(data, 'Entry', 'image')
        const fileOfImage =  getFieldOfObjectType(imageField, 'Asset', 'file')
        return 'https:' + fileOfImage.url;
    }

    const parsedData = {}
    if (isLoaded){
        parsedData['title'] = getField('title');
        parsedData['content'] = getField('content');
        parsedData['slug'] = getField('slug');
        if (isHasImage()) parsedData['image'] = getImage();
    };

    return isLoaded &&(
        <div className='pageContent'>
            {isHasImage() &&
                <div>
                    <div className='pageImageSpacer'/>
                    <div className='imageContainer' style={{backgroundImage: `url(${parsedData.image})`}}/>
                </div>
            }
            <div>
                <h1>{parsedData.title}</h1>
                {renderRichText(parsedData.content)}
            </div>
        </div>
    )
}

Page.propTypes ={
    slug: PropTypes.string.isRequired
}

export default Page;
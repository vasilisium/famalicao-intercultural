import {  useEffect, } from 'react';
import { createPortal } from 'react-dom';

import './portal.css';

const Portal = ({children, closeSideMenuHandler}) => {
    const mountElement = document.getElementById('portal');
    const portalContainer = document.createElement('div');
    mountElement.classList.add('backDropShadow');

    useEffect(()=>{
        mountElement.classList.add('backDropShadow')
        const escHandler = (e) => { if (e.keyCode === 27) closeSideMenuHandler(); }
        const clickHandler = (e) => {
            if (e.target!==mountElement) return;
            closeSideMenuHandler()
        }

        mountElement.appendChild(portalContainer);
        document.addEventListener('keydown', escHandler, false);
        mountElement.addEventListener('click', clickHandler);

        return () => {
            mountElement.classList.remove('backDropShadow');
            mountElement.removeChild(portalContainer);
            document.removeEventListener('keydown', escHandler, false);
            mountElement.removeEventListener('click', clickHandler);
        }

    }, [portalContainer, mountElement, closeSideMenuHandler]);

    return createPortal(children, portalContainer);
}


export default Portal;
import React from 'react';
import {TransitionGroup, CSSTransition} from 'react-transition-group';
import {Swipeable} from 'react-swipeable'

import './tripleSlider.css'

const TripleSlider = ({item1, item2, item3, moveLeftHandler, moveRightHandler, animationTimeout}) => {
    
    const at = animationTimeout || 300;
    
    return(
        <Swipeable onSwipedLeft={moveRightHandler} onSwipedRight={moveLeftHandler}>
        <div className='sliderContainer'>

            <div className='timelineButton tlb-left'onClick={moveLeftHandler}>
                <svg viewBox="0 0 256 512" height='24'>
                    <path 
                        fill="currentColor" 
                        d="M31.7 239l136-136c9.4-9.4 24.6-9.4 33.9 
                        0l22.6 22.6c9.4 9.4 9.4 24.6 0 33.9L127.9 256l96.4 96.4c9.4 9.4 9.4 24.6 0 33.9L201.7 
                        409c-9.4 9.4-24.6 9.4-33.9 0l-136-136c-9.5-9.4-9.5-24.6-.1-34z">
                    </path>
                </svg>
            </div> 

            <TransitionGroup className='slider'> 
                <CSSTransition classNames='anim' timeout={at} key={item1}>
                    <div className='element' onClick={moveLeftHandler}>
                        <div className='content'> {item1} </div>
                    </div>
                </CSSTransition>

                <CSSTransition classNames='anim' timeout={at} key={item2}>
                    <div className='element middle'>
                        <div className='content'> {item2} </div>
                    </div>
                </CSSTransition>

                <CSSTransition classNames='anim' timeout={at} key={item3}>
                    <div className='element' onClick={moveRightHandler} 
                        unselectable="on" 
                    >
                        <div className='content'> {item3} </div>
                    </div>
                </CSSTransition>
            </TransitionGroup>

            <div className='timelineButton tlb-right ' onClick={moveRightHandler}>
                <svg viewBox="0 0 256 512" height='24'>
                    <path 
                        fill="currentColor" 
                        d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 
                        0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 
                        103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z">
                    </path>
                </svg>
            </div>
        </div>
        </Swipeable>
    )
};

export default TripleSlider;
import React, {useEffect} from 'react';

import moment from 'moment';

import './event.css'

const Event = ({event}) => {
    const locationLink = () => {if (event.location) return `https://www.google.com/maps/search/?api=1&query=${event.location.lat},${event.location.lon}`};

    useEffect(()=>{
        window.history.pushState('object or string', 'Title', `/events/${event.slug}`);
    },[event])

    return (
        <div className='eventContainer'>
            {event.image 
                ? <div className='imageContainer' style={{backgroundImage: `url(${event.image})`}}/>
                : <div/>    
            }
            
            <div className='eventContent'>
                <div className='eventTitle'>{event.title}</div>

                <div className='eventData'>
                    <div className='eventDate'>{moment(event.date).format('dddd DD MMMM YYYY')}</div>
                    <a className='eventLocation' href={locationLink()} target="_blank" rel="noopener noreferrer">
                        <svg viewBox="0 0 384 512">
                            <path fill="currentColor" d="M172.268 501.67C26.97 291.031 0 269.413 
                                0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 
                                99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 
                                272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 
                                80 35.817 80 80 80z">
                            </path>
                        </svg>
                        check location
                    </a>
                </div>

                <div className='eventDescription'>{event.description}</div>
            </div>
        </div>
    )

}

export default Event;
import React from 'react';

import {SwitchTransition, CSSTransition} from 'react-transition-group';
import {Swipeable} from 'react-swipeable'
import moment from 'moment';

import MainContext from 'utils/context';

import TripleSlider from 'components/events/tripleSlider/tripleSlider';
import Event from 'components/events/event/event';
import PageNotFound from 'components/pageNotFound/pageNotFound';

import './eventsList.css';

class EventsList extends React.Component{

    static contextType = MainContext;

    constructor(props){
        super(props)
        this.state = {};
    }

    componentDidMount(){
        const api = this.context;

        api.initEvents(
            (curEvent)=>{
                this.setState({
                    cEvent: curEvent,
                    animationDirection: 'left', //'left', 'right'
                })
            },
            (er)=>{
                console.log(er)
            },
            this.props.slug
        )   
    }
  
    buttonClick(direction){
        // direction: 'left', 'right'

        const cbSucces = (res) => {
            if(!res) return null;
            
            if (direction === 'left'){
                this.setState({...this.state, cEvent: res.next})
                return res.next;
            }

            if(direction === 'right'){
                this.setState({...this.state, cEvent: res.prev})
                return res.prev;
            }
        }
        const cbError = (er) => {console.log(er)}

        this.setState({...this.state, animationDirection: direction}, ()=>{
            if(direction === 'left'){
                if (this.state.cEvent.prev) this.state.cEvent.prev.getPrevEvent(
                    ev=> {if(cbSucces(ev)===null) this.setState({...this.state, cEvent: this.state.cEvent.prev});},
                    cbError
                )
                return;
            }
            if(direction === 'right'){
                if (this.state.cEvent.next) this.state.cEvent.next.getNextEvent(
                    ev=> {if(cbSucces(ev)===null) this.setState({...this.state, cEvent: this.state.cEvent.next});},
                    cbError
                )
            }
        })
    }

    render(){
        const dates = [];
    
        if (this.state.cEvent){
            const events = this.state.cEvent.toArray();
            events.forEach(ev => {
                if (ev) dates.push(moment(ev.date).format('MMM DD'))
                else dates.push('');
            })
        }
        else {
            return <PageNotFound {...this.props}/>
        }

        return (
            <div className='eventsContainer'>
            {dates.length>0 &&
                <TripleSlider 
                    item1={dates[0]}
                    item2={dates[1]}
                    item3={dates[2]}
                    moveLeftHandler={()=>{
                        this.buttonClick('left');
                    }}
                    moveRightHandler={()=>{
                        this.buttonClick('right');
                    }}
                />
            }

            {this.state.cEvent && 
                <Swipeable onSwipedLeft={()=>this.buttonClick('right')} onSwipedRight={()=>this.buttonClick('left')} className='eventWrapper'>
                    <SwitchTransition mode='out-in'>
                        <CSSTransition classNames={`animated_${this.state.animationDirection}`} timeout={500} 
                            key={this.state.cEvent.slug}
                        >
                            <Event event={this.state.cEvent}/>
                        </CSSTransition>
                    </SwitchTransition>
                </Swipeable>
            }
        </div>
        )
    }
}

export default EventsList

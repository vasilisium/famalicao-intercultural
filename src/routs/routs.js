import React from 'react';
import {
    Route,
    Redirect,
    Switch,
} from "react-router-dom";

import config from 'utils/config';

import PageNotFound from 'components/pageNotFound/pageNotFound';
import Page from 'components/page/page';
import EventsList from 'components/events/eventsList/eventsList';
import BlogPage from 'components/blog/blogPage';
import PostPage from 'components/blog/postPage';

const renderRouts = (links) => (
    <Switch>
        <Route exact path="/">
                <Redirect to={config.pages.startPage}/>
        </Route>

        <Route path='/events/:slug' 
            render={(props)=>{
                return <EventsList {...props} slug={props.match.params.slug}/>;
            }}
        />
        <Route path='/events/' component={EventsList}/>
        <Route path='/blog/:slug' 
            render={(props)=>{
                return <PostPage {...props} slug={props.match.params.slug}/>;
            }}
            // component={PostPage}
        />
        <Route path='/blog/' component={BlogPage}/>
        <Route
            path='/:slug'
            render={(props)=>{
                if (Object.keys(links).includes(props.match.params.slug)) return <Page slug={props.match.params.slug}/>
                return <PageNotFound {...props} />
            }}
        />

        <Route component={PageNotFound}/>
    </Switch>
);

export default renderRouts;
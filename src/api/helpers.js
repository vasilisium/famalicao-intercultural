export const isObjectHasField = (object, fieldName) => {
    const pr = Object.getOwnPropertyNames(object.fields);
    const res = pr.includes(fieldName);
    return res
}

export const isTypeMatching = (object, typeName) => {
    try {
        return getType(object) === typeName;
    } catch {
        return false
    }
}

export const getType = (object) => {
    try {
        return object.sys.type;
    } catch {
        return undefined
    }
}


export const getFieldOfObjectType = (object, typeName, fieldName) => {
    try {
        if (isTypeMatching(object, typeName)) {
            if(fieldName ==='id') return object.sys.id;
            return object.fields[fieldName]
        }
    } catch (error) {
        console.log(error);
        throw new Error('Error while trying accessing specified field.')
    }
}

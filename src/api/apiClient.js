import {createClient} from 'contentful';

import {
    getFieldOfObjectType, 
} from './helpers'

import Event from './event';
import Blog from 'api/blog/blog';

export default class APIClient {
    
    // settings: {key, mainMenu_entryId}
    // key: { space, accessToken}
    constructor(settings, cbSuccess, cbError){
        this.mainMenu_entryId = settings.mainMenu_entryId;
        this.initKey(settings.key);
        this.links = {};
        this.mainMenu = {};
        this.getMainMenuPages(cbSuccess, cbError)
        this.blog = undefined; 
    }

    initKey({space, accessToken}){
        this.client = createClient({space, accessToken});
    }

    getResultsWhenQueriedEntries=(response)=>response.items

    // MainMenu
    getMainMenuPages(cbSuccess, cbError){
        this.client.getEntry(this.mainMenu_entryId)
        .then(res => {
            // const pages = {};
            getFieldOfObjectType(res, 'Entry', 'links').forEach(link => {

                this.links[getFieldOfObjectType(link, 'Entry', 'slug')] = link;

                const label = getFieldOfObjectType(link, 'Entry', 'title') || getFieldOfObjectType(link, 'Entry', 'label');
                const slug = getFieldOfObjectType(link, 'Entry', 'slug');

                this.mainMenu[slug] = {
                    name: label,
                    id: slug,
                }
            });
            cbSuccess(this.pages);
        })
        .catch(error => cbError(error))
    }
    
    isPageLoaded(slug){
        return Object.getOwnPropertyNames(this.links).includes(slug);
    }

    getPage(slug, cbSuccess, cbError){
        if (this.isPageLoaded(slug)) {
            cbSuccess(this.links[slug]);
            return;
        }

        this.getPages((pages) =>{
            if (this.isPageLoaded(slug)) 
                cbSuccess(pages[slug])
            else 
                cbError(new Error(`There is no such page, with slug: ${slug}`))
        }, cbError)
    }

    getPages(cbSuccess, cbError){
        this.client.getEntries({content_type: 'page'})
        .then(res => {
            res.items.forEach(page => {
                const slug = this.getFieldOfObjectType(page, 'Entry', 'slug');
                if (!this.isPageLoaded(slug)) this.pages[slug] = page
            });
            cbSuccess(this.pages);
        })
        .catch(error => cbError(error))
    }

    // Events
    initEvents = (cbSuccess, cbError, slug) => new Event(this, cbSuccess, cbError, undefined, undefined, undefined, slug);
    initBlog = (cbSuccess, cbError, slug) => { this.blog = new Blog(this, cbSuccess, cbError, slug); }
}


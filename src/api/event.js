import {emptyCallBack, getFormatedDate} from 'utils/functions';
import {getFieldOfObjectType} from 'api/helpers';

class Event{
    constructor(api, cbSuccess, cbError, entry, prev, next, slug){
        this.client = api.client;
        this.api = api;
        this.contentType = 'event'

        this.entry = entry;
        this.prev = prev;
        this.next = next;
        this.slug = slug;

        if(this.entry===undefined){
            this.init(
                (res) => {
                    this.entry=res
                    this.assignEntry();

                    cbSuccess(this);
                },
                cbError,
                slug
            );
        } else this.assignEntry();
    }

    assignEntry(){
        if(this.entry===undefined) return;

        const getField = (fieldName) => getFieldOfObjectType(this.entry, 'Entry', fieldName)

        if (!this.slug) this.slug = getField('slug');

        this.title = getField('title');
        this.date = getField('date');
        this.description = getField('description');
        this.location = getField('location');

        const imageField =  getField('image');
        if (imageField) this.image = 'https:' + getFieldOfObjectType(imageField, 'Asset', 'file').url;
        else this.image = undefined;
    }

    init(cbSuccess, cbError, slug){
        let query = { 
            limit: 1, 
            content_type: this.contentType,
        }

        if (slug) {
            query = {
                ...query,
                'fields.slug': slug,
            }
        } else {
            query = {
                ...query,
                'fields.date[gte]': getFormatedDate(new Date()),
                order: 'fields.date',
            }
        }

        this.getQueryableEvent(
            query,
            res=>{
                if(!res) {
                    cbError(new Error(`event "${slug}" not founded`))
                    return;
                }
                this.entry=res;
                this.assignEntry();
                
                const queries = [
                    this.getPrevEvent(()=>{},()=>{},),
                    this.getNextEvent(()=>{},()=>{},),
                ]

                const getResult = (array, index) => {
                    if(!array) return undefined;
                    if((array.length-1)>=index) {return array[index]}
                    else return undefined;
                }

                // type ['prev', 'next']
                const createEvent = (type, results) =>{
                    if(type==='prev'){
                        const entry = getResult(getResult(results, 0).items, 0)
                        if(entry) {return new Event(this.api, ()=>{}, ()=>{}, entry, undefined, this)}
                        else return undefined;
                    }
                    if(type==='next'){
                        const entry = getResult(getResult(results, 1).items, 0)
                        if(entry) {return new Event(this.api, ()=>{}, ()=>{}, entry, this)}
                        else return undefined;
                    }
                }

                Promise.all(queries)
                .then(results=>{

                    this.prev = createEvent('prev', results);
                    this.next = createEvent('next', results);

                    cbSuccess(res);
                })
                .catch(er=>console.log(er))
            },
            cbError
        )

    }

    getQueryableEvent(query, cbSuccess, cbError){
        const p = this.client.getEntries(query);

        if (cbSuccess) p.then(res=>cbSuccess(this.api.getResultsWhenQueriedEntries(res)[0]))
        if (cbError) p.catch(cbError);

        return p;
    }

    getPrevEvent(cbSuccess, cbError){
        if(this.prev) {
            cbSuccess(this.prev);
            return this.prev;
        }

        const query = {
            content_type: this.contentType,
            'fields.date[lt]': getFormatedDate(new Date(this.date)),
            limit: 1,
            order: '-fields.date',
        }
        
        const p = this.getQueryableEvent(
            query,
            cbSuccess!==undefined && (
            res=>{
                let e = undefined
                if(res!==undefined){
                    e = new Event(
                        this.api,
                        emptyCallBack,
                        emptyCallBack,
                        res,
                        undefined,
                        this,
                    )
                }
                this.prev=e;
                if(cbSuccess) cbSuccess(e);
            }),
            er=>{if(cbError) cbError(er)}
        )

        return p;
    }

    getNextEvent(cbSuccess, cbError){
        if(this.next) {
            cbSuccess(this.next);
            return this.next;
        }

        const query = {
            content_type: this.contentType,
            'fields.date[gt]': getFormatedDate(new Date(this.date)),
            limit: 1,
            order: 'fields.date',
        }

        const p = this.getQueryableEvent(
            query,
            cbSuccess!==undefined && (
            res=>{
                let e = undefined
                if(res!==undefined){
                    e = new Event(
                        this.api,
                        emptyCallBack,
                        emptyCallBack,
                        res,
                        this
                    )
                }
                this.next=e;
                if(cbSuccess) cbSuccess(e);
            }),
            er=>{if(cbError) cbError(er)}
        )

        return p;
    }

    toArray = () => [this.prev, this, this.next]
}

export default Event;
import {getFormatedDate} from 'utils/functions';

class Blog{
    constructor(api, cbSuccess, cbError, slug){
        this.api = api;
        this.contentType = 'post';

        this.posts = [];
        this.slugs = {};
        // this.dates = {};
        
        if(slug){
            this.getPosts(
                ()=>this.getPostBySlug(cbSuccess, cbError, slug),
                cbError,
            );
        }
        else this.getPosts(cbSuccess, cbError);
    }

    setPosts(posts_request_results){
        posts_request_results.items.forEach(post => {
            this.posts.push(post)
            this.slugs[post.fields.slug] = post;
            // this.dates[post.sys.createdAt] = post; 
        });
    }
    getPosts(cbSuccess, cbError, date){

        const dateParamKey = 'sys.createdAt';
        const dateParamVal = date ? getFormatedDate(date) : getFormatedDate(new Date());
        const dateParam = {[dateParamKey+'[lte]']: dateParamVal};

        let query = { 
            limit: 3, 
            content_type: this.contentType,
            order: '-sys.createdAt',
        };

        query = {...query, ...dateParam};

        const p = this.api.client.getEntries(query);

        if (cbSuccess) p.then(res=>{
            this.setPosts(res);
            cbSuccess(this.posts);
        })
        if (cbError) p.catch(cbError);

        return p;
    }

    isPostLoaded(slug){
        return Object.getOwnPropertyNames(this.slugs).includes(slug);
    }

    getPostBySlug(cbSuccess, cbError, slug){
        if(this.isPostLoaded(slug)) {
            cbSuccess(this.slugs[slug]);
            return;
        }
        
        let query = { 
            limit: 1, 
            content_type: this.contentType,
            'fields.slug': slug
        };

        this.api.client.getEntries(query)
        .then(res=>{
            if (res.items.length>0) cbSuccess(res.items[0])
            else cbError(new Error('no '))
        })
        .catch(cbError);
    }

    async getLastPostDate(){
        const query = { 
            limit: 1, 
            content_type: this.contentType,
            order: '-sys.createdAt',
            select: 'sys.createdAt'

        };

        const res = await this.api.client.getEntries(query)
        .then(result=>result)
        .catch(err=>new Error(`Error while requesting date of the last post: \n${err}`))

        if(res instanceof Error) return res;
        return res.items[0].sys.createdAt;
    }

    loadPosts(){

    }

}

export default Blog;
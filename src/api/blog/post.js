import {getFieldOfObjectType} from 'api/helpers';

class Post{
    constructor(api, cbSuccess, cbError, entry, slug){
        this.client = api.client;
        this.api = api;
        this.contentType = 'post'

        this.entry = entry;
        this.slug = slug;

        if(this.entry===undefined){
            this.init(
                (res) => {
                    this.entry=res
                    this.assignEntry();

                    cbSuccess(this);
                },
                cbError,
                slug
            );
        } else this.assignEntry();
    }

    assignEntry(){
        if(this.entry===undefined) return;

        const getField = (fieldName) => getFieldOfObjectType(this.entry, 'Entry', fieldName)

        this.title = getField('title');
        this.text = getField('description');
        this.slug = getField('slug');
        this.posted = undefined;

        const imagesField =  getField('images');
        // if (imageField) this.image = 'https:' + getFieldOfObjectType(imageField, 'Asset', 'file').url;
        // else this.image = undefined;
    }

}

export default Post;
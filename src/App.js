import React, {
    useState, 
    useEffect,
    // useRef,
} from 'react';
import {BrowserRouter as Router} from "react-router-dom";

import 'App.css';

import config from 'utils/config';
import MainContext from 'utils/context';
import Api from 'api/apiClient';

import Portal from 'components/portal/portal';
import Header from 'components/header/header';

import renderRouts from 'routs/routs';

const App = () => {

    const [api, setApi] = useState(null);
    const [error, setError] = useState(null);
    const [isFetchng, setIsFetching] = useState(true);
    const [isPortalShowing , setPortalShowing] = useState(false);
    const [portalContent, setPortalContent] = useState(null);

    useEffect(() =>{
        if (isFetchng) {
            const newApi = new Api(config.contentful,
                () =>{
                    setApi(newApi);
                    setIsFetching(false);
                },
            setError)}
        }, 
        [ 
            api, 
            isFetchng, 
        ]
    );
    
    if (error) console.log(error);

    const setPortalWithContent = (content, isShow) => {
        if (isPortalShowing) {setPortalContent(null)} 
        else {setPortalContent(content)};
        
        if (isShow!==undefined)  {setPortalShowing(isShow)} 
        else {setPortalShowing(!isPortalShowing)};
    }

    const appContent = (isFetchng, api) => isFetchng ? null : (
        <Router><MainContext.Provider value={api}>
            {isPortalShowing && ( 
                <Portal closeSideMenuHandler = {()=>{ setPortalWithContent(undefined, false) }}>
                    {portalContent} 
                </Portal>
            )}
            <Header {...config.title} 
                menuButtonClickHandler = {(content)=>{setPortalWithContent(content, true)}}
                closeMenuButtonHandler = {()=>{ 
                    setPortalWithContent(null, false)
                }}
            />
            
            {renderRouts(api.links)}

        </MainContext.Provider></Router>
    )
    
    return appContent(isFetchng, api)
}

export default App;

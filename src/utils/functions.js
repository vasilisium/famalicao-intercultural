import React from 'react';

import {Link} from 'react-router-dom'
import {INLINES} from '@contentful/rich-text-types';
import {documentToReactComponents} from '@contentful/rich-text-react-renderer'
import moment from 'moment'

export const emptyCallBack = () => {}
export const getFormatedDate = (date, withTime = true) =>{
    //2021-05-10T19:00
    const m=date.getMonth() + 1;
    const d=date.getDate();
    const h=date.getHours();
    const min=date.getMinutes();
    return `${
            date.getFullYear()
        }-${
            (m<=9?'0'+m:m)
        }-${
            (d<=9?'0'+d:d)
        }${withTime?`T${h<=9?'0'+h:h}:${min<=9?'0'+min:min}`:''}`;
}
export const creationDate = entry => moment(entry.sys.createdAt).format('DD.MM.YYYY HH:mm');


export const renderRichText = (contentfulRichText) => (
    documentToReactComponents(contentfulRichText, {
        renderNode: {
            [INLINES.HYPERLINK]: (props) => <Link to={props.data.uri}>{props.content[0].value}</Link>
        }
    })
)

const config = {
    minLoadTime: 3000,
    title: {
        title: 'Famalicao intercultural',
        slogan: 'we bring people together'
    },
    contentful: {
        key:{
            space: process.env.REACT_APP_SPACE_KEY,
            accessToken: process.env.REACT_APP_ACCESS_TOKEN,
        },
        mainMenu_entryId: '4SIV6k0gd4zR3V18ZjfI86'
    },
    pages:{
        startPage: '/main',
    },
    
    sessionAnimationKey: 'animationPayed',
}

export default config;